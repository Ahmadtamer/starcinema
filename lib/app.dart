import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:star_cinema/core/constants/app_routes.dart';
import 'package:star_cinema/core/constants/app_themes.dart';
import 'package:star_cinema/core/enums/app_languages.dart';
import 'package:star_cinema/core/helper/app_localizations.dart';
import 'package:star_cinema/core/helper/route_generator.dart';

import 'core/screen_util_init.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenInit(
      builder: (BuildContext context, Widget? widget) {
        RouteGenerator generator = RouteGenerator();
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          supportedLocales: AppLanguages.values.map((e) => Locale(e.value)),
          localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            DefaultCupertinoLocalizations.delegate
          ],
          onGenerateRoute: generator.generateRoute,
          initialRoute: AppRoute.splashScreen,
          theme: Themes.light(context),
        );
      },
    );
  }
}
