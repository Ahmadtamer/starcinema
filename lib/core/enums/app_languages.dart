import 'package:star_cinema/core/constants/app_image_paths.dart';
import 'package:star_cinema/core/constants/app_strings.dart';

enum AppLanguages {
  english,
  germany,
  spanish,
}

extension AppLanguageHelper on AppLanguages {
  String get value {
    switch (this) {
      case AppLanguages.germany:
        return 'de';
      case AppLanguages.english:
        return 'en';
      case AppLanguages.spanish:
        return 'es';
    }
  }

  String get name {
    switch (this) {
      case AppLanguages.germany:
        return AppStrings.deutsch;
      case AppLanguages.english:
        return AppStrings.english;
      case AppLanguages.spanish:
        return AppStrings.espanol;
      default:
        return AppStrings.english;
    }
  }

  String get icon {
    switch (this) {
      case AppLanguages.germany:
        return AppImages.germany;
      case AppLanguages.english:
        return AppImages.english;
      case AppLanguages.spanish:
        return AppImages.spanish;
    }
  }
}

extension LanguageHelper on String {
  String getLanguageName() {
    switch (this) {
      case 'de':
        return AppStrings.deutsch;
      case 'en':
        return AppStrings.english;
      case 'es':
        return AppStrings.espanol;
      default:
        return AppStrings.english;
    }
  }

  String getLanguageImage() {
    switch (this) {
      case 'de':
        return AppImages.germany;
      case 'en':
        return AppImages.english;
      case 'es':
        return AppImages.spanish;
      default:
        return AppImages.english;
    }
  }
}
