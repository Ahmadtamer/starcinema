abstract class AppStrings {
  static const String appName = "AppName";
  static const String required = "Required";
  static const String emailNotValid = "NotValidEmail";
  static const String passwordNotValid = "passwordNotValid";
  static const String emailOrPasswordIsNotValid = "emailOrPasswordIsNotValid";

  static const String startWithUpper = "startWithUpper";
  static const String nameLengthError = "nameLengthError";
  static const String validEmail = "validEmail";
  static const String validMobile = "validMobile";

  static const String deutsch = "deutsch";
  static const String english = "english";
  static const String espanol = "espanol";
  static const String onlyLetters = "onlyLetters";
  static const String phoneNumberNotValid = "phoneNumberNotValid";

  static const noInternet = 'NoInternet';
  static  const search = 'Search';
  static const cancel = 'Cancel';
  static  const name = 'Name';
  static  const description = 'Description';
  static const ingredients = 'Ingredients';
  static  const favorites = 'Favorites';
  static const cannotBeNull = 'CannotBeNull';
  static const email = 'Email';
  static const password = 'Password';
  static const login = 'Login';
  static const continueAsGuest = 'ContinueAsGuest';
  static const notHaveAccount = 'NotHaveAccount';
  static const passwordLengthError = 'PasswordLengthError';
  static const notValidEmail = 'NotValidEmail';
  static  const authNotCorrect = 'AuthNotCorrect';

}
