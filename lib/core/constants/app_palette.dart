import 'package:flutter/material.dart';

enum PaletteType { light, dark }

class AppPalette {
  static const Color primaryColor = Color.fromARGB(255, 30, 42, 99);
  static const Color black = Color.fromARGB(255, 0, 0, 0);
  static const Color white = Color.fromARGB(255, 255, 255, 255);

  static const Color primaryBackground = Color(0xffF1F2E7);
  static const Color primaryGreen = Color(0xff7EBB42);
  static const Color lightRed = Color(0xffFC7462);
  static const Color red = Color(0xffC92C2C);
  static const Color brown = Color(0xff783D3E);
  static const Color darkBrown = Color(0xff473D32);
  static const Color lightBrown = Color(0xff9D8C69);
  static const Color darkGrey = Color(0xff343A40);
  static const Color grey = Color(0xff9BB9C3);
  static const Color defaultGrey = Colors.grey;
  static const Color lightGrey = Color(0xffF5F5F5);
}
