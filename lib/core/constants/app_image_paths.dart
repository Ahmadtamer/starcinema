abstract class AppImages {
  static const String _base = "assets/";
  static const String _images = "${_base}images/";
  static const String _icons = "${_base}icons/";

  // images
  static const String logo = '${_images}app_icon.png';
  static const noInternetImg = '${_images}no_internet.png';

  // icons
  static const String english = '${_icons}english.png';
  static const String germany = '${_icons}germany.png';
  static const String spanish = '${_icons}spanish.png';
  static const String male = '${_icons}male.svg';
  static const String female = '${_icons}female.svg';
  static const String popularity = '${_icons}popularity.svg';
  static const String star = '${_icons}star.svg';
  static const String adult = '${_icons}18-plus.svg';
}
