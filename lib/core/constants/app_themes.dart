import 'package:flutter/material.dart';
import 'package:star_cinema/core/constants/app_palette.dart';

class Themes {
  static ThemeData light(BuildContext context) => ThemeData(
        fontFamily: 'Roboto',
        primaryColor: AppPalette.primaryColor,
        accentColor: AppPalette.black,
        canvasColor: AppPalette.white,
        brightness: Brightness.light,
        splashColor: Colors.transparent,
        scaffoldBackgroundColor: AppPalette.primaryBackground,
        appBarTheme: const AppBarTheme(
          brightness: Brightness.light,
          color: AppPalette.primaryBackground,
        ),
        textTheme: const TextTheme()
            .apply(
              displayColor: AppPalette.black,
              bodyColor: AppPalette.black,
            )
            .copyWith(
              bodyText2: const TextStyle(
                  fontSize: 16,
                  color: AppPalette.black,
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0.5,
                  height: 1.3),
            ),
        dividerColor: AppPalette.black.withOpacity(0.2),
        buttonColor: AppPalette.primaryColor,
        buttonTheme: const ButtonThemeData(
          height: 60,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
        ),
      );

  static ThemeData dark(BuildContext context) => ThemeData(
        fontFamily: 'Roboto',
        primaryColor: AppPalette.lightBrown,
        accentColor: AppPalette.white,
        canvasColor: AppPalette.primaryColor,
        brightness: Brightness.dark,
        splashColor: Colors.transparent,
        scaffoldBackgroundColor: AppPalette.black,
        appBarTheme: const AppBarTheme(
          color: Colors.transparent,
          brightness: Brightness.dark,
        ),
        textTheme: const TextTheme()
            .apply(
              bodyColor: AppPalette.white,
              displayColor: AppPalette.white,
            )
            .copyWith(
                bodyText2: TextStyle(
              color: AppPalette.white.withOpacity(0.9),
              fontWeight: FontWeight.w400,
              fontSize: 16,
              letterSpacing: 0.5,
              height: 1.3,
            )),
        dividerColor: AppPalette.white.withOpacity(0.2),
        buttonColor: AppPalette.white,
        buttonTheme: const ButtonThemeData(
          height: 60,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(8),
            ),
          ),
        ),
      );
}
