class AppRoute {
  static const String splashScreen = '/splashScreen';
  static const String homeScreen = '/homeScreen';
  static const String detailsScreen = '/detailsScreen';
  static const String movieDetailsScreen = '/movieDetailsScreen';
}
