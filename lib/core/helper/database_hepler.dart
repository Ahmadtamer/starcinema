import 'dart:async';
import 'dart:io' as io;

import 'package:injectable/injectable.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';

@Injectable()
class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database? _db;

  Future<Database> get db async {
    if (_db != null) return _db!;
    _db = await initDb();
    return _db!;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE People(id INTEGER PRIMARY KEY, name TEXT, profile_path TEXT)");
  }

  Future<int> addFavPeople(People people) async {
    var dbClient = await db;
    People myPeople = People(
        id: people.id, name: people.name, profilePath: people.profilePath);
    int res = await dbClient.insert("People", myPeople.toJson());
    return res;
  }

  Future<List<People>> getAllPeople() async {
    var dbClient = await db;
    List<Map> list = await dbClient.rawQuery('SELECT * FROM People');
    List<People> peopleList = [];
    for (int i = 0; i < list.length; i++) {
      var people = People(
        id: list[i]["id"],
        name: list[i]["name"],
        profilePath: list[i]["profilePath"],
      );
      peopleList.add(people);
    }
    print(peopleList.length);
    return peopleList;
  }

  Future<int> removeFavPeople(People people) async {
    var dbClient = await db;

    int res = await dbClient
        .rawDelete('DELETE FROM People WHERE id = ?', [people.id]);
    return res;
  }

  Future<bool> peopleExists(People people) async {
    var dbClient = await db;

    var result = await dbClient.rawQuery(
      'SELECT EXISTS(SELECT 1 FROM People WHERE id = ?)',
      [people.id],
    );
    int? exists = Sqflite.firstIntValue(result);
    return (exists == 1);
  }
}
