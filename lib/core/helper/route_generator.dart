import 'package:flutter/material.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/constants/app_routes.dart';
import 'package:star_cinema/features/get_people/data/model/known_fore_model.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';
import 'package:star_cinema/core/helper/database_hepler.dart';
import 'package:star_cinema/features/get_people/presenters/pages/details_screen.dart';
import 'package:star_cinema/features/get_people/presenters/pages/home_screen.dart';
import 'package:star_cinema/features/get_people/presenters/pages/movie_details.dart';
import 'package:star_cinema/core/splash_screen.dart';

class RouteGenerator {
  late DatabaseHelper db;

  RouteGenerator() {
    db = DatabaseHelper();
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    switch (settings.name) {
      case AppRoute.splashScreen:
        return _screenInit(const SplashScreen(), settings);

      case AppRoute.homeScreen:
        return _screenInit(const HomeScreen(), settings);

      case AppRoute.detailsScreen:
        final person = settings.arguments as People;
        return _screenInit(DetailsScreen(person, db), settings);

      case AppRoute.movieDetailsScreen:
        final KnownFor knownFor = settings.arguments as KnownFor;
        return _screenInit(MovieDetailsScreen(knownFor: knownFor), settings);

      default:
        return _errorRoute();
    }
  }

  static MaterialPageRoute<dynamic> _screenInit(
      Widget screen, RouteSettings settings) {
    return MaterialPageRoute<dynamic>(
        builder: (_) => screen, settings: settings);
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute<dynamic>(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: AppPalette.primaryColor,
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
