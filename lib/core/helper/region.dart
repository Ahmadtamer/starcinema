enum RegionType { EU, US }

RegionType regionType = RegionType.US;

extension AppRegionHelper on RegionType {
  String get currency {
    switch (this) {
      case RegionType.EU:
        return '€';
      case RegionType.US:
        return '\$';
    }
  }
}
