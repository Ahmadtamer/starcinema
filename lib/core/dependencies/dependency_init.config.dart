// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/get_people/domain/repositories/local_data_source/get_people_local_source.dart'
    as _i5;
import '../../features/get_people/domain/repositories/remote_data_source/get_people_remote_source.dart'
    as _i6;
import '../../features/get_people/domain/usecases/get_people_usecase.dart'
    as _i7;
import '../../features/get_people/presenters/bloc/get_popular/get_popular_people_cubit.dart'
    as _i8;
import '../apis/api_service.dart' as _i3;
import '../helper/database_hepler.dart'
    as _i4; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.ApiService>(() => _i3.ApiService());
  gh.factory<_i4.DatabaseHelper>(() => _i4.DatabaseHelper());
  gh.factory<_i5.GetPeopleLocalSource>(
      () => _i5.GetPeopleLocalSource(get<_i4.DatabaseHelper>()));
  gh.factory<_i6.GetPeopleRemoteSource>(
      () => _i6.GetPeopleRemoteSource(get<_i3.ApiService>()));
  gh.factory<_i7.GetPeopleUseCase>(() => _i7.GetPeopleUseCase(
      get<_i6.GetPeopleRemoteSource>(), get<_i5.GetPeopleLocalSource>()));
  gh.singleton<_i8.GetPopularPeopleCubit>(
      _i8.GetPopularPeopleCubit(get<_i7.GetPeopleUseCase>()));
  return get;
}
