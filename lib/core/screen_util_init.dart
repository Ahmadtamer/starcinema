import 'package:flutter/widgets.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ScreenInit extends StatelessWidget {
  const ScreenInit({Key? key, required this.builder}) : super(key: key);

  final Widget Function(BuildContext, Widget?) builder;

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(1209.60, 872),
      builder: builder,
    );
  }
}
