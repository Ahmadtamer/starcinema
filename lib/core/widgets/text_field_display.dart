import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/helper/app_localizations.dart';

class AppEditText extends StatelessWidget {
  final Color textColor;
  final Color? hintColor;
  final Color? backgroundColor;
  final Color borderColor;
  final double? fontSize;
  final String? text;
  final String? translation;
  final FontWeight? fontWeight;
  final String fontFamily;
  final double radius;
  TextStyle? style;
  TextInputType keyboardType;
  bool isHint;
  final TextAlign textAlign;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  EdgeInsets? contentPadding;
  final TextDecoration? textDecoration;
  final InputDecoration? decoration;
  final TextEditingController? controller;
  final bool autoFocus;
  final bool alignLabelWithHint;
  final bool enableInteractiveSelection;
  final int maxLength;
  final ValueChanged<String>? onChanged;
  final FocusNode? focusNode;
  final String? errorText;
  final bool obscureText;
  final bool readOnly;
  InputBorder? border;
  FormFieldValidator<String>? validator;
  TextInputAction? textInputAction;
  List<TextInputFormatter>? inputFormatters;
  ValueChanged<String>? onSubmitted;

  AppEditText({
    this.textColor = AppPalette.white,
    this.backgroundColor,
    this.fontSize,
    this.text,
    this.validator,
    this.prefixIcon,
    this.readOnly = false,
    this.fontFamily = 'Roboto',
    this.textDecoration,
    this.borderColor = Colors.transparent,
    this.focusNode,
    this.radius = 8,
    this.onChanged,
    this.controller,
    this.keyboardType = TextInputType.text,
    this.decoration,
    this.translation,
    this.isHint = true,
    this.style,
    this.contentPadding,
    this.textAlign = TextAlign.start,
    this.fontWeight,
    this.autoFocus = false,
    this.alignLabelWithHint = false,
    this.enableInteractiveSelection = true,
    this.suffixIcon,
    this.maxLength = 10,
    this.errorText,
    this.obscureText = false,
    this.hintColor,
    this.textInputAction,
    this.inputFormatters,
    this.onSubmitted,
  });

  @override
  Widget build(BuildContext context) {
    init();
    return TextFormField(
      autofocus: autoFocus,
      validator: validator,
      readOnly: readOnly,
      maxLength: maxLength,
      onChanged: onChanged,
      focusNode: focusNode,
      obscureText: obscureText,
      controller: controller,
      keyboardType: keyboardType,
      autocorrect: false,
      textInputAction: textInputAction,
      inputFormatters: inputFormatters,
      onFieldSubmitted: onSubmitted,
      decoration: decoration ??
          InputDecoration(
              contentPadding: contentPadding,
              counterText: "",
              errorText: errorText,
              suffixIcon: suffixIcon,
              suffix: suffixIcon,
              prefixIcon: prefixIcon,
              border: border,
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: borderColor, width: .8),
                borderRadius: BorderRadius.all(Radius.circular(radius)),
              ),
              fillColor: backgroundColor ?? AppPalette.white.withOpacity(0.3),
              focusColor: AppPalette.primaryColor,
              enabledBorder: border,
              filled: true,
              hintStyle: TextStyle(color: hintColor),
              hintText: isHint
                  ? translation != null
                      ? AppLocalizations.of(context)?.translate(translation!)
                      : text ?? ''
                  : null,
              alignLabelWithHint: alignLabelWithHint),
      textAlign: textAlign,
      style: style ??
          TextStyle(
              decoration: textDecoration,
              fontSize: fontSize?.sp,
              color: textColor,
              fontFamily: fontFamily,
              fontWeight: fontWeight),
    );
  }

  void init() {
    border = const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(6)),
      borderSide: BorderSide.none,
    );
  }
}
