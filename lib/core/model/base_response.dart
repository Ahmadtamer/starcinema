import 'ApiResponse.dart';

class AppResponse<T extends BaseResponse> {
  bool? success;
  int? statusCode;
  int? page;
  int? totalPages;
  int? totalResults;
  String? statusMessage;
  T? result;
  List<T>? results;

  AppResponse();

  AppResponse.fromJson(Map<String, dynamic>? json, BaseResponse object,
      {bool isList = false}) {
    if (json != null) {
      success = json['Success'];
      statusCode = json['status_code'];
      page = json['page'];
      totalPages = json['total_pages'];
      totalResults = json['total_results'];
      statusMessage = json['status_message'].toString();
      if (isList) {
        results = (resultsMap(json) as List)
            .map((e) => e == null
                ? null
                : object.fromJson(e as Map<String, dynamic>) as T)
            .cast<T>()
            .toList();
      } else {
        result = resultMap(json) == null
            ? null
            : object.fromJson(resultMap(json) as Map<String, dynamic>);
      }
    }
  }

  dynamic resultsMap(Map<String, dynamic> json) {
    if (json['results'] == null) return json['results'];
    return json['results'];
  }

  dynamic resultMap(Map<String, dynamic> json) {
    if (json['result'] == null) return json['result'];
    return json['result'];
  }
}
