import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class ApiService {
  late Dio _dio;
  final baseUrl = 'https://api.themoviedb.org/3/';
  final apiKey = '8b8789264ffd29149e52eb5f2dfe3fac';

  ApiService() {
    BaseOptions options = BaseOptions(
      baseUrl: baseUrl,
      queryParameters: {"api_key": apiKey, "language": "en"},
      receiveDataWhenStatusError: true,
      connectTimeout: 20 * 1000,
      // 60 seconds,
      receiveTimeout: 20 * 1000,
    );

    _dio = Dio(options);
  }

  Future<Response<T>> postApi<T>(
    String path, {
    Map<String, dynamic> body = const {},
  }) async =>
      await _dio.post(path, data: body);

  Future<Response<T>> getApi<T>(
    String path, {
    Map<String, dynamic> queryParameters = const {},
  }) async =>
      _dio.get(path, queryParameters: queryParameters);

  Future<Response<T>> putApi<T>(
    String path, {
    Map<String, dynamic> body = const {},
  }) async =>
      await _dio.put(path, data: body);
}
