import 'dart:async';

import 'package:flutter/material.dart';
import 'package:star_cinema/core/constants/app_image_paths.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/constants/app_routes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(seconds: 3), (timer) async {
      Navigator.pushReplacementNamed(context, AppRoute.homeScreen);
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      backgroundColor: AppPalette.primaryBackground,
      body: Center(child: Image.asset(AppImages.logo)));
}
