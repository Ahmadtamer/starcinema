import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_cinema/features/get_people/presenters/bloc/get_popular/get_popular_people_cubit.dart';

import 'dependencies/dependency_init.dart';

class AppMainBlocProvider extends StatelessWidget {
  AppMainBlocProvider({Key? key, this.child}) : super(key: key);
  final GetPopularPeopleCubit _getPopularPeoplesCubit =
      getIt<GetPopularPeopleCubit>();
  final Widget? child;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<GetPopularPeopleCubit>(
            create: (BuildContext context) => _getPopularPeoplesCubit),
      ],
      child: child!,
    );
  }
}
