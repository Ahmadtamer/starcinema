import 'dart:async';

import 'package:flutter/material.dart';
import 'package:star_cinema/core/bloc_provider.dart';
import 'package:star_cinema/core/dependencies/dependency_init.dart';

import 'app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  configureDependencies();

  runZonedGuarded(() async {
    runApp(AppMainBlocProvider(child: const MyApp()));
  }, (_, __) {});
}
