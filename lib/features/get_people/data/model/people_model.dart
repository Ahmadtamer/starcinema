import 'package:star_cinema/core/model/ApiResponse.dart';
import 'package:star_cinema/features/get_people/data/model/known_fore_model.dart';

class People extends BaseResponse {
  bool? adult;
  int? gender;
  int? id;
  List<KnownFor>? knownFor;
  String? knownForDepartment;
  String? name;
  double? popularity;
  String? profilePath;

  People(
      {this.adult,
      this.gender,
      this.id,
      this.knownFor,
      this.knownForDepartment,
      this.name,
      this.popularity,
      this.profilePath});

  People.init();

  @override
  fromJson(Map<String, dynamic> json) => People.fromJson(json);

  People.fromJson(Map<String, dynamic> json) {
    adult = json['adult'];
    gender = json['gender'];
    id = json['id'];
    if (json['known_for'] != null) {
      knownFor = <KnownFor>[];
      json['known_for'].forEach((v) {
        knownFor!.add(KnownFor.fromJson(v));
      });
    }
    knownForDepartment = json['known_for_department'];
    name = json['name'];
    popularity = json['popularity'];
    profilePath = json['profile_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (adult != null) data['adult'] = adult;
    if (gender != null) data['gender'] = gender;
    if (id != null) data['id'] = id;
    if (knownFor != null) {
      data['known_for'] = knownFor!.map((v) => v.toJson()).toList();
    }
    if (knownForDepartment != null) {
      data['known_for_department'] = knownForDepartment;
    }
    if (name != null) data['name'] = name;
    if (popularity != null) data['popularity'] = popularity;
    if (profilePath != null) data['profile_path'] = profilePath;
    return data;
  }
}
