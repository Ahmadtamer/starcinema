import 'package:injectable/injectable.dart';
import 'package:star_cinema/core/helper/database_hepler.dart';
import 'package:star_cinema/core/model/base_response.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';

@Injectable()
class GetPeopleLocalSource {
  final DatabaseHelper db;

  GetPeopleLocalSource(this.db);

  Future<AppResponse<People>> getPopularPeople() async {
    AppResponse<People> people = AppResponse();
    try {
      people.results = await db.getAllPeople();
    } catch (e) {
      people.success = false;
    }
    return people;
  }

  Future<void> addPopularPeople(List<People> people) async {
    for (People item in people) {
      await db.addFavPeople(item);
    }
  }
}
