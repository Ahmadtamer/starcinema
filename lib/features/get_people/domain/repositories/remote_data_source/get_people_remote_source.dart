import 'package:injectable/injectable.dart';
import 'package:star_cinema/core/apis/api_paths.dart';
import 'package:star_cinema/core/apis/api_service.dart';
import 'package:star_cinema/core/model/base_response.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';

@Injectable()
class GetPeopleRemoteSource {
  final ApiService apiService;

  GetPeopleRemoteSource(this.apiService);

  Future<AppResponse<People>> getPopularPeoples(int page) async {
    var response = await apiService
        .getApi(ApiPaths.popular, queryParameters: {"page": page});
    return AppResponse<People>.fromJson(response.data, People.init(),
        isList: true);
  }
}
