import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:star_cinema/core/model/base_response.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';
import 'package:star_cinema/features/get_people/domain/repositories/local_data_source/get_people_local_source.dart';
import 'package:star_cinema/features/get_people/domain/repositories/remote_data_source/get_people_remote_source.dart';

@Injectable()
class GetPeopleUseCase {
  final GetPeopleRemoteSource getPeopleRemoteSource;
  final GetPeopleLocalSource getPeopleLocalSource;

  GetPeopleUseCase(this.getPeopleRemoteSource, this.getPeopleLocalSource);

  Future<AppResponse<People>> getPeople(bool isConnected, int page) async {
    if (kDebugMode) {
      print(isConnected);
    }
    if (isConnected) {
      final result = await getPeopleRemoteSource.getPopularPeoples(page);
      if (result.results?.isNotEmpty ?? false) {
        getPeopleLocalSource.addPopularPeople(result.results!);
      }
      return result;
    } else {
      return getPeopleLocalSource.getPopularPeople();
    }
  }
}
