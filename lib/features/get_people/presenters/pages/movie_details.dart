import 'package:cached_network_image/cached_network_image.dart';
import 'package:fl_downloader/fl_downloader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:star_cinema/core/constants/app_image_paths.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/widgets/app_size_boxes.dart';
import 'package:star_cinema/core/widgets/loading.dart';
import 'package:star_cinema/core/widgets/size_extension.dart';
import 'package:star_cinema/core/widgets/text_display.dart';
import 'package:star_cinema/features/get_people/data/model/known_fore_model.dart';

class MovieDetailsScreen extends StatefulWidget {
  final KnownFor knownFor;

  const MovieDetailsScreen({Key? key, required this.knownFor})
      : super(key: key);

  @override
  State<MovieDetailsScreen> createState() => _MovieDetailsScreenState();
}

class _MovieDetailsScreenState extends State<MovieDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [buildSliverAppBar(), buildSliverList()],
        ),
      ),
    );
  }

  Widget buildDownload() {
    return Container(
      decoration:
          const BoxDecoration(color: AppPalette.brown, shape: BoxShape.circle),
      child: IconButton(
        onPressed: () => FlDownloader.download(
            "https://image.tmdb.org/t/p/original${widget.knownFor.posterPath!}"),
        icon: const Icon(Icons.download, color: AppPalette.white),
      ),
    );
  }

  Widget buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 0.5.sh,
      pinned: true,
      stretch: true,
      backgroundColor: AppPalette.primaryBackground,
      iconTheme: const IconThemeData(color: AppPalette.brown),
      flexibleSpace: Stack(alignment: Alignment.bottomCenter, children: [
        buildCachedNetworkImage(),
        buildDownload(),
      ]),
    );
  }

  Widget buildName() {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: AppTextDisplay(
        text: widget.knownFor.title,
        fontWeight: FontWeight.w700,
        color: AppPalette.lightGrey,
        textAlign: TextAlign.start,
        fontSize: 24,
      ),
    );
  }

  Widget buildCachedNetworkImage() {
    return FlexibleSpaceBar(
      title: buildName(),
      centerTitle: true,
      background: Hero(
        tag: widget.knownFor.id!,
        child: (widget.knownFor.posterPath == null)
            ? const Icon(Icons.error)
            : Padding(
                padding: const EdgeInsets.all(24.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(90),
                  child: CachedNetworkImage(
                    height: double.infinity,
                    placeholder: (context, url) => const LoadingWidget(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    imageUrl:
                        "https://image.tmdb.org/t/p/original${widget.knownFor.posterPath!}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
      ),
    );
  }

  SliverList buildSliverList() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildFirstRow(),
                16.heightBox,
                buildMovieDescription(),
                50.heightBox,
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildFirstRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildVoteAverage(),
        if (widget.knownFor.adult ?? false) buildAdult(),
        buildReleaseDate(),
      ],
    );
  }

  Widget buildAdult() =>
      SvgPicture.asset(AppImages.adult, width: 24, color: AppPalette.brown);

  Row buildVoteAverage() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        AppTextDisplay(
          text: widget.knownFor.voteAverage.toString(),
          fontWeight: FontWeight.w300,
          color: AppPalette.brown,
          textAlign: TextAlign.start,
          fontSize: 18,
        ),
        16.widthBox,
        SvgPicture.asset(AppImages.star, width: 20, color: AppPalette.brown),
      ],
    );
  }

  AppTextDisplay buildReleaseDate() {
    return AppTextDisplay(
      text: widget.knownFor.releaseDate,
      fontWeight: FontWeight.w300,
      color: AppPalette.brown,
      textAlign: TextAlign.start,
      fontSize: 18,
    );
  }

  Widget buildMovieDescription() {
    return AppTextDisplay(
      text: widget.knownFor.overview,
      fontWeight: FontWeight.w700,
      color: AppPalette.brown,
      textAlign: TextAlign.start,
      maxLines: 20,
      fontSize: 18,
    );
  }
}
