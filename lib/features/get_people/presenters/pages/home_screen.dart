import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/constants/app_routes.dart';
import 'package:star_cinema/core/constants/app_strings.dart';
import 'package:star_cinema/core/widgets/bottom_loader.dart';
import 'package:star_cinema/core/widgets/loading.dart';
import 'package:star_cinema/core/widgets/text_display.dart';
import 'package:star_cinema/core/widgets/text_field_display.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';
import 'package:star_cinema/features/get_people/presenters/bloc/get_popular/get_popular_people_cubit.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isSearching = false;
  final _searchTextController = TextEditingController();
  List<People> peopleList = [];
  List<People> searchedPeople = [];

  final _scrollController = ScrollController();
  bool _isConnected = false;

  @override
  void initState() {
    _scrollController.addListener(_onScroll);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_onScroll)
      ..dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom && _isConnected) _fetchPage();
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }

  _fetchPage() => BlocProvider.of<GetPopularPeopleCubit>(context)
      .getGetPopularPeoples(_isConnected);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppPalette.lightGrey,
      appBar: buildAppBar(),
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          _isConnected = connectivity != ConnectivityResult.none;
          if (kDebugMode) {
            print(_isConnected);
          }
          _fetchPage();
          return buildBlocWidget();
        },
        child: const LoadingWidget(),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: AppPalette.lightGrey,
      leading: _isSearching ? const BackButton(color: AppPalette.brown) : null,
      centerTitle: true,
      title: _isSearching ? _buildSearchField() : _buildAppBarTitle(),
      actions: _buildAppBarActions(),
    );
  }

  Widget buildBlocWidget() {
    return BlocBuilder<GetPopularPeopleCubit, GetPopularPeopleState>(
      builder: (context, state) {
        if (state is GetPopularPeopleSuccess) {
          peopleList = (state).people;
          return buildPeopleList(state);
        } else if (state is GetPopularPeopleInitial) {
          return const LoadingWidget();
        } else if (state is GetPopularPeopleFailed) {
          return const Center(child: Text('failed to fetch posts'));
        } else {
          return Container();
        }
      },
    );
  }

  Widget _buildAppBarTitle() => AppTextDisplay(
        translation: AppStrings.appName,
        color: AppPalette.brown,
        fontSize: 20,
      );

  Widget _buildSearchField() => AppEditText(
        controller: _searchTextController,
        translation: AppStrings.search,
        prefixIcon: const Icon(Icons.search),
        onChanged: (searchedPeople) {
          addSearchedFOrItemsToSearchedList(searchedPeople);
        },
      );

  List<Widget> _buildAppBarActions() {
    if (_isSearching) {
      return [
        InkWell(
          onTap: () {
            _clearSearch();
            Navigator.pop(context);
          },
          child: Padding(
            padding:
                EdgeInsets.symmetric(horizontal: ScreenUtil().setHeight(8)),
            child: Center(
                child: AppTextDisplay(
              translation: AppStrings.cancel,
              color: AppPalette.red,
              fontSize: 12,
            )),
          ),
        ),
      ];
    } else {
      return [
        IconButton(
          onPressed: _startSearch,
          icon: const Icon(Icons.search, color: AppPalette.brown),
        ),
      ];
    }
  }

  void _startSearch() {
    ModalRoute.of(context)!
        .addLocalHistoryEntry(LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void _stopSearching() {
    _clearSearch();

    setState(() {
      _isSearching = false;
    });
  }

  void _clearSearch() {
    setState(() {
      _searchTextController.clear();
    });
  }

  void addSearchedFOrItemsToSearchedList(String searchedItem) {
    searchedPeople = peopleList
        .where((people) => people.name!.toLowerCase().startsWith(searchedItem))
        .toList();
    setState(() {});
  }

  Widget buildPeopleList(GetPopularPeopleSuccess state) {
    return ListView.builder(
      shrinkWrap: true,
      controller: _scrollController,
      itemCount: _searchTextController.text.isEmpty
          ? peopleList.length
          : searchedPeople.length,
      itemBuilder: (ctx, index) {
        return index >= state.people.length
            ? const BottomLoader()
            : peopleItem(
                _searchTextController.text.isEmpty
                    ? peopleList[index]
                    : searchedPeople[index],
              );
      },
    );
  }

  Widget peopleItem(People character) {
    return InkWell(
      onTap: () => Navigator.pushNamed(context, AppRoute.detailsScreen,
          arguments: character),
      child: _searchTextController.text.isEmpty
          ? defaultCharacterItem(character)
          : searchedCharacterItem(character),
    );
  }

  Widget defaultCharacterItem(People character) {
    return Stack(alignment: Alignment.bottomLeft, children: [
      cachedNetworkImage(character),
      Container(
        color: AppPalette.primaryBackground,
        padding: const EdgeInsets.all(16),
        child: AppTextDisplay(
          text: character.name,
          fontWeight: FontWeight.w300,
          color: AppPalette.brown,
        ),
      ),
    ]);
  }

  Widget cachedNetworkImage(People character) {
    if (character.profilePath == null) {
      return const Icon(Icons.error);
    } else {
      return Hero(
        tag: character.id!,
        child: CachedNetworkImage(
          width: double.infinity,
          placeholder: (context, url) => const LoadingWidget(),
          errorWidget: (context, url, error) => const Icon(Icons.error),
          imageUrl:
              "https://image.tmdb.org/t/p/original${character.profilePath!}",
          fit: BoxFit.cover,
        ),
      );
    }
  }

  Widget searchedCharacterItem(People character) {
    return SizedBox(
      height: ScreenUtil().setHeight(60),
      child: Row(
        children: [
          SizedBox(
              width: ScreenUtil().setWidth(60),
              child: cachedNetworkImage(character)),
          Expanded(child: AppTextDisplay(text: character.name))
        ],
      ),
    );
  }
}
