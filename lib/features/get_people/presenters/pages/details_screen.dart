import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:star_cinema/core/constants/app_image_paths.dart';
import 'package:star_cinema/core/constants/app_palette.dart';
import 'package:star_cinema/core/constants/app_routes.dart';
import 'package:star_cinema/core/helper/database_hepler.dart';
import 'package:star_cinema/core/widgets/app_size_boxes.dart';
import 'package:star_cinema/core/widgets/loading.dart';
import 'package:star_cinema/core/widgets/text_display.dart';
import 'package:star_cinema/features/get_people/data/model/known_fore_model.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';

class DetailsScreen extends StatefulWidget {
  final People people;
  final DatabaseHelper db;

  const DetailsScreen(this.people, this.db, {Key? key}) : super(key: key);

  @override
  State<DetailsScreen> createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: [buildSliverAppBar(), buildSliverList()],
        ),
      ),
    );
  }

  Widget buildGenderIcon() {
    return Container(
      padding: const EdgeInsets.all(12),
      decoration:
          const BoxDecoration(color: AppPalette.brown, shape: BoxShape.circle),
      child: SvgPicture.asset(
        widget.people.gender == 0 ? AppImages.male : AppImages.female,
        color: AppPalette.white,
        width: 24,
        height: 24,
      ),
    );
  }

  Widget buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 0.5.sh,
      pinned: true,
      stretch: true,
      backgroundColor: AppPalette.primaryBackground,
      iconTheme: const IconThemeData(color: AppPalette.brown),
      flexibleSpace: Stack(alignment: Alignment.bottomCenter, children: [
        buildCachedNetworkImage(widget.people),
        buildGenderIcon(),
      ]),
    );
  }

  Widget buildName() {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: AppTextDisplay(
        text: widget.people.name,
        fontWeight: FontWeight.w700,
        color: AppPalette.lightGrey,
        textAlign: TextAlign.start,
        fontSize: 24,
      ),
    );
  }

  Widget buildCachedNetworkImage(People people) {
    return FlexibleSpaceBar(
      title: buildName(),
      centerTitle: true,
      background: Hero(
        tag: widget.people.id!,
        child: (people.profilePath == null)
            ? const Icon(Icons.error)
            : Padding(
                padding: const EdgeInsets.all(24.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(90),
                  child: CachedNetworkImage(
                    height: double.infinity,
                    placeholder: (context, url) => const LoadingWidget(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                    imageUrl:
                        "https://image.tmdb.org/t/p/original${people.profilePath!}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
      ),
    );
  }

  SliverList buildSliverList() {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                buildFirstRow(),
                if (widget.people.knownFor?.isNotEmpty ?? false) ...[
                  24.heightBox,
                  buildKnownForTitle(),
                  16.heightBox,
                  buildKnownForImages(),
                ],
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildKnownForTitle() {
    return AppTextDisplay(
      text: "Known For:",
      fontWeight: FontWeight.w700,
      color: AppPalette.brown,
      textAlign: TextAlign.start,
      fontSize: 18,
    );
  }

  Widget buildFirstRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildDepartment(),
        buildPopularity(),
      ],
    );
  }

  Row buildPopularity() {
    return Row(
      children: [
        AppTextDisplay(
          text: widget.people.popularity.toString(),
          fontWeight: FontWeight.w300,
          color: AppPalette.brown,
          textAlign: TextAlign.start,
          fontSize: 18,
        ),
        4.widthBox,
        SvgPicture.asset(
          AppImages.popularity,
          color: AppPalette.brown,
        )
      ],
    );
  }

  AppTextDisplay buildDepartment() {
    return AppTextDisplay(
      text: widget.people.knownForDepartment,
      fontWeight: FontWeight.w300,
      color: AppPalette.brown,
      textAlign: TextAlign.start,
      fontSize: 18,
    );
  }

  Widget buildKnownForImages() {
    return SizedBox(
      height: 0.4.sh,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.people.knownFor!.length,
        itemBuilder: (context, index) => Container(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          width: 0.35.sw,
          child: buildKnownForItem(widget.people.knownFor![index]),
        ),
      ),
    );
  }

  Widget buildKnownForItem(KnownFor knownFor) {
    return InkWell(
      onTap: () => {
        Navigator.pushNamed(context, AppRoute.movieDetailsScreen,
            arguments: knownFor)
      },
      child: Column(
        children: [
          if (knownFor.posterPath != null) buildKnownForImg(knownFor),
          8.heightBox,
          buildKnownForName(knownFor),
        ],
      ),
    );
  }

  Widget buildKnownForName(KnownFor knownFor) {
    return AppTextDisplay(
      text: knownFor.title,
      fontWeight: FontWeight.w300,
      color: AppPalette.brown,
      textAlign: TextAlign.start,
      fontSize: 14,
    );
  }

  Widget buildKnownForImg(KnownFor knownFor) {
    return Hero(
      tag: knownFor.id!,
      child: SizedBox(
        height: 0.3.sh,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: CachedNetworkImage(
            height: double.infinity,
            placeholder: (context, url) => const LoadingWidget(),
            errorWidget: (context, url, error) => const Icon(Icons.error),
            imageUrl:
                "https://image.tmdb.org/t/p/original${knownFor.posterPath!}",
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
