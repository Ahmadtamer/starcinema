part of 'get_popular_people_cubit.dart';

@immutable
abstract class GetPopularPeopleState {}

class GetPopularPeopleInitial extends GetPopularPeopleState {}

class GetPopularPeopleSuccess extends GetPopularPeopleState {
  final List<People> people;

  GetPopularPeopleSuccess(this.people);
}

class GetPopularPeopleFailed extends GetPopularPeopleState {
  final String error;

  GetPopularPeopleFailed(this.error);
}
