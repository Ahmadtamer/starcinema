import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:star_cinema/core/model/base_response.dart';
import 'package:star_cinema/features/get_people/data/model/people_model.dart';
import 'package:star_cinema/features/get_people/domain/usecases/get_people_usecase.dart';

part 'get_popular_people_state.dart';

@singleton
class GetPopularPeopleCubit extends Cubit<GetPopularPeopleState> {
  final GetPeopleUseCase getPeopleUseCase;
  List<People> people = [];
  int page = 1;

  GetPopularPeopleCubit(this.getPeopleUseCase)
      : super(GetPopularPeopleInitial());

  Future<void> getGetPopularPeoples(bool isConnected) async {
    try {
      AppResponse<People> response =
          await getPeopleUseCase.getPeople(isConnected, page);
      if (response.results?.isNotEmpty ?? false) {
        people.addAll(response.results ?? []);
        page++;
        emit(GetPopularPeopleSuccess(people));
      } else {
        emit(GetPopularPeopleFailed("Error in getGetPopularPeoples"));
      }
    } catch (e) {
      print(e.toString());
      emit(GetPopularPeopleFailed(e.toString()));
    }
  }
}
